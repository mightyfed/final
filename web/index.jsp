<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><fmt:message key="local.title"/></title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
<div class="container w60">
    <jsp:include page="header.jsp"/>

    <div class="main-container">
        <fmt:message key="local.text"/>
        <c:if test="${not empty user}">, ${user.name}</c:if>
        !
    </div>
    <jsp:include page="footer.jsp"/>
</div>
</body>
</html>