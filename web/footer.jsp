<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<div class="row">
<footer>
	<span class="author"> <fmt:message key="local.author"/></span>
</footer>