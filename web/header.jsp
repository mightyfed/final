<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<div class="row">
  <div class="col-md-1 logo"></div>
  <div class="col-md-4 textlogo">
    <h1>Communal services</h1>
  </div>
  <div class="col-md-1 col-md-offset-4 signin">
    <button type="button" class="btn btn-info mybtn">
      <c:if test="${empty user && empty admin}">
        <a href="login.jsp"> <fmt:message key="local.login"/></a>
      </c:if>
      <c:if test="${ not empty user || not empty admin}">
        <a href="logout"> <fmt:message key="local.logout"/></a>
      </c:if>
    </button>
    <c:if test="${empty user && empty admin}">
      <button type="button" class="btn-info mybtn2">
        <a href="register.jsp"> <fmt:message key="local.register"/></a>
      </button>
    </c:if>
    <button type="button" class="btn-info mybtn3">
      <a href="changeloc"><fmt:message key="local.btnloc"/></a>
    </button>
  </div>
</div>

<div class="row">
  <nav class="main-menu">
    <ul>
      <li><a href="home"> <fmt:message key="local.homepaje"/></a></li>
      <c:if test="${not empty user}">
        <li><a href="work"> <fmt:message key="local.work"/></a></li>
      </c:if>
      <c:if test="${not empty admin}">
        <li><a href="worker"> <fmt:message key="local.workers"/></a></li>
      </c:if>
      <c:if test="${not empty user}">
        <li><a href="my_requests"><fmt:message key="local.myrequests"/></a></li>
      </c:if>
      <c:if test="${not empty admin}">
        <li><a href="show_requests"><fmt:message key="local.allrequests"/></a></li>
      </c:if>
    </ul>
  </nav>
</div>