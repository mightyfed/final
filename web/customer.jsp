<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<!doctype html>
<html>
<head>
  <title><fmt:message key="local.customers"/></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/table.css">
</head>
<body>
<div class="container w60">
  <jsp:include page="header.jsp" />
  <c:if test="${not empty user}">
  <div class="main-container">
    <div class="date">
      <p><fmt:message key="local.hist"/></p>
      <table class="table table-condensed">
        <tr>
          <th>Id</th>
          <th><fmt:message key="local.reqwork"/></th>
          <th><fmt:message key="local.reqdate"/></th>
        </tr>
        <c:forEach items="${myReq}" var="mreq">
          <tr>
            <td>${mreq.id_request}</td>
            <td>${mreq.reqwork}</td>
            <td>${mreq.reqtime}</td>
          </tr>
        </c:forEach>
      </table>
    </div>
    <div class="date">
      <p>N<fmt:message key="local.newreq"/></p>
      <form action="create_request" method="post">
        <select name="work" required="">
          <option disabled selected value=""><fmt:message key="local.chreqw"/></option>
          <c:forEach items="${work}" var="wk">
            <option>${wk.name}</option>
          </c:forEach>
        </select><br>
        <input type="date" name="date" placeholder="Required date"><br>
        <input type="submit" value="Send">
      </form>
    </div>
    <div class="date">
      <p><fmt:message key="local.delreq"/></p>
      <form action="delete_request" method="post">
        <select name="request" required="">
          <option disabled selected value=""><fmt:message key="local.chreqreq"/></option>
          <c:forEach items="${myReq}" var="mrk">
            <option>${mrk.id_request}</option>
          </c:forEach>
        </select>
        <input type="submit" value="Delete">
      </form>
    </div>
  </div>
  </c:if>
  <jsp:include page="footer.jsp" />
</div>
</body>
</html>