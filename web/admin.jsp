<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<!doctype html>
<html>
<head>
  <title><fmt:message key="local.requests"/></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/table.css">
</head>
<body>
<div class="container w60">
  <jsp:include page="header.jsp" />
  <c:if test="${not empty admin}">
  <div class="main-container">
    <div class="date">
      <p><fmt:message key="local.listnewreq"/></p>
      <table class="table table-condensed">
        <tr>
          <th>Id</th>
          <th><fmt:message key="local.reqwork"/></th>
          <th><fmt:message key="local.reqdate"/></th>
        </tr>
        <c:forEach items="${nReq}" var="nreq">
          <tr>
            <td>${nreq.id_request}</td>
            <td>${nreq.reqwork}</td>
            <td>${nreq.reqtime}</td>
            <td></td>
          </tr>
        </c:forEach>
      </table>
    </div>
    <div class="date">
      <p><fmt:message key="local.listexpreq"/></p>
      <table class="table table-condensed">
        <tr>
          <th>Id</th>
          <th><fmt:message key="local.reqwork"/></th>
          <th><fmt:message key="local.reqdate"/></th>
        </tr>
        <c:forEach items="${eReq}" var="ereq">
          <tr>
            <td>${ereq.id_request}</td>
            <td>${ereq.reqwork}</td>
            <td>${ereq.reqtime}</td>
            <td></td>
          </tr>
        </c:forEach>
      </table>
    </div>
    <div class="date">
      <p>Assign worker</p>
      <form action="assign_worker" method="post">
        <input type="number" name="request" required="" placeholder="Request ID"><br>
        <input type="number" name="worker" required="" placeholder="Worker ID"><br>
        <input type="submit" value= <fmt:message key="local.assign"  /><br>
      </form>
    </div>
  </div>
  </c:if>
  <jsp:include page="footer.jsp" />
</div>
</body>
</html>