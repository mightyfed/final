<%@page contentType="text/html; utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<!DOCTYPE html>
<html>
<head>
  <title><fmt:message key="local.login"/></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
</head>

<body>
<div class="container w60">
  <jsp:include page="header.jsp" />
  <div class="main-container">
    <div class="row">
      <div class="col-md-12">
        <h1><fmt:message key="local.login"/></h1>
      </div>
    </div>
    <br> <br>
    <form class="form-horizontal" action="login" method="POST">
      <div class="form-group">
        <label for="inputEmail3" class="col-md-2 control-label">E-mail</label>
        <div class="col-md-5">
          <input type="email" class="form-control" id="inputEmail3"
                 placeholder="E-mail" name="email">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-md-2 control-label"><fmt:message key="local.pass"/></label>
        <div class="col-md-5">
          <input type="password" class="form-control" id="inputPassword3"
                 placeholder="Password" name="password">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-6 control-label">
          <c:if test="${ not empty loginError}"><fmt:message key="local.errlog"/></c:if>
        </label>
      </div>
      <div class="form-group">
        <div class="col-md-offset-2 col-md-1">
          <button type="submit" class="btn btn-default"><fmt:message key="local.login"/></button>
        </div>
        <div class="col-md-1">
          <button type="button" class="btn btn-default">
            <a href="index.jsp"><fmt:message key="local.back"/></a>
          </button>
        </div>
      </div>
    </form>
  </div>
  <jsp:include page="footer.jsp" />
</div>

</body>
</html>