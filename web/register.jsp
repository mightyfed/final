<%@page contentType="text/html; utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.epam.training.resourse.local" />
<!DOCTYPE html>
<html>
<head>
  <title><fmt:message key="local.regist"/></title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
</head>

<body>
<div class="container w60">
  <jsp:include page="header.jsp" />
  <div class="main-container">
    <div class="row">
      <div class="col-md-12">
        <h1>Register</h1>
      </div>
    </div>
    <br> <br>
    <form class="form-horizontal" action="register" method="POST">
      <div class="form-group">
        <label for="exampleInputName2" class="col-md-2 control-label"><fmt:message key="local.name"/></label>
        <div class="col-md-5">
          <input type="text" class="form-control" id="exampleInputName2"
                 placeholder="Name" name="name">
        </div>
      </div>

      <div class="form-group">
        <label for="inputAddress" class="col-md-2 control-label"><fmt:message key="local.address"/></label>
        <div class="col-md-5">
          <input type="text" class="form-control" id="inputAddress"
                 placeholder="Address" name="address">
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-md-2 control-label">E-mail</label>
        <div class="col-md-5">
          <input type="email" class="form-control" id="inputEmail3"
                 placeholder="E-mail" name="email">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-md-2 control-label"><fmt:message key="local.pass"/></label>
        <div class="col-md-5">
          <input type="password" class="form-control" id="inputPassword3"
                 placeholder="Password" name="password">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-6 control-label">
          <c:if test="${ not empty registrationError}"><fmt:message key="local.regerr1"/></c:if>
        </label>
      </div>
      <div class="form-group">
        <label class="col-md-6 control-label">
          <c:if test="${ not empty registrationError1}"><fmt:message key="local.regerr2"/>!</c:if>
        </label>
      </div>
      <div class="form-group">
        <div class="col-md-offset-2 col-md-1">
          <button type="submit" class="btn btn-default"><fmt:message key="local.register"/></button>
        </div>
        <div class="col-md-1">
          <button type="button" class="btn btn-default">
            <a href="index.jsp">Back</a>
          </button>
        </div>
      </div>
    </form>
  </div>
  <jsp:include page="footer.jsp" />
</div>

</body>
</html>