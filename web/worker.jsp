
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Workers</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/table.css">
</head>
<body>
<div class="container w60">
  <jsp:include page="header.jsp" />
  <div class="main-container">
    <div class="date">
      <p>List of workers</p>
      <table class="table table-condensed">
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Busy</th>
        </tr>
        <c:forEach items="${workers}" var="wks">
          <tr>
            <td>${wks.id_worker}</td>
            <td>${wks.name}</td>
            <td>${wks.is_busy}</td>
          </tr>
        </c:forEach>
      </table>
    </div>
  </div>
  <jsp:include page="footer.jsp" />
</div>
</body>
</html>