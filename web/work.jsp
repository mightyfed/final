
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Work</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="css/table.css">
</head>
<body>
<div class="container w60">
  <jsp:include page="header.jsp" />

  <div class="main-container">
    <div class="actions"></div>
    <div class="date">
      <table class="table table-condensed">
        <tr>
          <th>Kind of work</th>
          <th>Price</th>
        </tr>
        <c:forEach items="${work}" var="wk">
          <tr>
            <td>${wk.name}</td>
            <td>${wk.price}</td>
          </tr>
        </c:forEach>
      </table>
    </div>
  </div>
  <jsp:include page="footer.jsp" />
</div>
</body>
</html>
