package by.epam.training.entity;

public class Work {

	private int id_work;

	private String name;

	private int price;

	public Work(int id_work, String name, int price) {
		this.id_work = id_work;
		this.name = name;
		this.price = price;
	}

	public Work() {

	}

	public int getId_work() {
		return this.id_work;
	}

	public String getName() {
		return this.name;
	}

	public int getPrice() {
		return this.price;
	}
}
