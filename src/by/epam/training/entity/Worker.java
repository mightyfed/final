package by.epam.training.entity;

public class Worker {

	private int id_worker;
	private String name;
	private boolean is_busy;
	private int id_work;

	public Worker() {

	}

	public Worker(int id_worker, String name, boolean is_busy, int id_work) {
		this.id_worker = id_worker;
		this.name = name;
		this.is_busy = is_busy;
		this.id_work = id_work;
	}

	public int getId_worker() {
		return this.id_worker;
	}

	public String getName() {
		return this.name;
	}

	public boolean getIs_busy() {
		return this.is_busy;
	}

	public int getId_work() {
		return this.id_work;
	}
}
