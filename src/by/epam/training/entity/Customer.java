package by.epam.training.entity;

public class Customer {

	private int id_customer;
	private String name;
	private String address;
	private String email;
	private String password;

	public Customer(int id_customer, String name, String address, String email,
			String password) {
		this.id_customer = id_customer;
		this.name = name;
		this.address = address;
		this.email = email;
		this.password = password;
	}

	public Customer() {

	}

	public int getId_customer() {
		return this.id_customer;
	}

	public String getEmail() {
		return this.email;
	}

	public String getPassword() {
		return this.password;
	}

	public String getName() {
		return this.name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}
}
