package by.epam.training.entity;

public class Request {

	private int id_request;
	private int id_customer;
	private Integer id_worker;
	private String reqwork;
	private String reqtime;

	public Request() {

	}

	public Request(int id_customer, String reqwork, String reqtime) {
		this.id_customer = id_customer;
		this.id_worker = null;
		this.reqwork = reqwork;
		this.reqtime = reqtime;
	}

	public Request(int id_request, int id_customer, int id_worker, String reqwork, String reqtime) {
		this.id_request = id_request;
		this.id_customer = id_customer;
		this.id_worker = id_worker;
		this.reqwork = reqwork;
		this.reqtime = reqtime;
	}

	public int getId_request() {
		return this.id_request;
	}

	public int getId_customer() {
		return this.id_customer;
	}

	public int getId_worker() {
		return this.id_worker;
	}

	public String getReqwork() {
		return this.reqwork;
	}

	public String getReqtime() {
		return this.reqtime;
	}
}
