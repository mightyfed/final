package by.epam.training.dao.connectionpool;

import by.epam.training.dao.DAOException;

public class ConnectionPoolException extends DAOException {

 private static final long serialVersionUID = 20L;

 public ConnectionPoolException(String msg, Exception e) {

  super(msg, e);

 }

 public ConnectionPoolException(String message) {
  super(message);
 }

}