package by.epam.training.dao;

public final class DAOString {
    public static final String ID_CUSTOMER = "id_customer";
    public static final String NAME = "name";
    public static final String ADDRESS= "address";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String ID_REQUEST = "id_request";
    public static final String REQUIRED_WORK = "reqwork";
    public static final String REQUIRED_TIME = "reqtime";
    public static final String IS_BUSY = "is_busy";
    public static final String ID_WORK = "id_work";
    public static final String PRICE = "price";
    public static final String ID_WORKER = "id_worker";

}
