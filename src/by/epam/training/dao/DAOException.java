package by.epam.training.dao;

import by.epam.training.command.CommandException;


/**
 * Class DAOException
 *
 * @author Fiodor
 */

public class DAOException extends CommandException {

    private static final long serialVersionUID = 2L;

    public DAOException(String msg, Exception e) {

        super(msg, e);

    }

    public DAOException(String message) {
        super(message);
    }

}
