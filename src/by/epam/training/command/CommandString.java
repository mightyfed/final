package by.epam.training.command;

public final class CommandString {
    public static final String ADMIN = "admin";
    public static final String ADDRESS= "address";
    public static final String WORKER = "worker";
    public static final String REQUEST = "request";
    public static final String NEW_REQUEST_ERROR = "Can't get new requests";
    public static final String EXPIRED_REQUEST_ERROR = "Can't get expired requests";
    public static final String DATE= "date";
    public static final String ASSIGN_ERROR = "assignErr";
    public static final String USER = "user";
    public static final String WORK = "work";
    public static final String ERROR = "error";
    public static final String MY_REQUEST = "myReq";
    public static final String MY_REQUEST_ERROR_ATTRIBUTE = "myReqErr";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String LOGIN_ERROR1 = "loginError";
    public static final String LOGIN_ERROR2 = "Wrong email or password!";
    public static final String NAME = "name";
    public static final String REGISTRATION_ERROR = "registrationError";
    public static final String REGISTRATION_ERROR1 = "registrationError1";
    public static final String REGISTRATION_ERROR2 = "Don't register!";
}
