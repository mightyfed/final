package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAORequest;
import by.epam.training.dao.inpl.DAODBRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteRequestCommand extends Command {

    private DAORequest dReq = new DAODBRequest();


    public  String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{

        int request_id = Integer.valueOf(request.getParameter(CommandString.REQUEST));
        try {
            dReq.deleteRequest(request_id);
        } catch (DAOException ex) {
            throw new CommandException(ex.getMessage());
        }

        return JspPageName.CUSTOMER_PAGE;
    }


}
