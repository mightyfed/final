package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAORequest;
import by.epam.training.dao.DAOWorker;
import by.epam.training.dao.inpl.DAODBRequest;
import by.epam.training.dao.inpl.DAODBWorker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class AssignWorkerCommand extends Command {
;
    private DAORequest daoRq = new DAODBRequest();
    private DAOWorker dWorker = new DAODBWorker();


    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        int request_id = Integer.valueOf(request.getParameter(CommandString.REQUEST));
        int worker_id = Integer.valueOf(request.getParameter(CommandString.WORKER));
        try {

            int res = daoRq.updateRequest(request_id, worker_id);
            if (res == 0) {
                request.getSession().setAttribute(CommandString.ASSIGN_ERROR, CommandString.ASSIGN_ERROR);

            } else {
                request.getSession().setAttribute(CommandString.ASSIGN_ERROR, null);
                dWorker.updateWorker(worker_id);
            }
        } catch (DAOException ex) {
            throw new CommandException(ex.getMessage());
        }
        return JspPageName.ADMIN_PAGE;
    }
}
