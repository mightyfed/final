package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOWorker;
import by.epam.training.dao.inpl.DAODBWorker;
import by.epam.training.entity.Worker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GetWorkerListCommand extends Command {
	private DAOWorker dWorker = new DAODBWorker();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{
		HttpSession session = request.getSession();
		List<Worker> workers = null;
		try {
			workers = dWorker.getAllWorkers();
		} catch (DAOException ex) {
			throw new CommandException(ex.getMessage());
		}
		session.setAttribute("workers", workers);

		return JspPageName.WORKER_PAGE;
	}
}
