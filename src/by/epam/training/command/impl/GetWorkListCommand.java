package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOWork;
import by.epam.training.dao.inpl.DAODBWork;
import by.epam.training.entity.Work;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GetWorkListCommand extends Command {
	private DAOWork dWork = new DAODBWork();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{
		HttpSession session = request.getSession();

		List<Work> work = null;
		try {
			work = dWork.getWork();
		} catch (DAOException ex) {
			throw new CommandException(ex.getMessage());
		}
		session.setAttribute(CommandString.WORK, work);
		return JspPageName.WORK_PAGE;
	}
}
