package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAORequest;
import by.epam.training.dao.inpl.DAODBRequest;
import by.epam.training.entity.Customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CreateRequestCommand extends Command  {
	private DAORequest dReq = new DAODBRequest();


	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{
		HttpSession session = request.getSession();

		Customer cust = (Customer) session.getAttribute(CommandString.USER);
		String work = request.getParameter(CommandString.WORK);
		String date = request.getParameter(CommandString.DATE);
		try {
			dReq.addRequest(cust.getId_customer(), work, date);
		} catch (DAOException ex) {
			throw new CommandException(ex.getMessage());
		}

		return JspPageName.CUSTOMER_PAGE;
	}
}
