package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.inpl.DAODBCustomer;
import by.epam.training.entity.Customer;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class RegisterCommand extends Command {


    private DAODBCustomer DAOCustomer =new DAODBCustomer();

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        Customer customer = null;
        String email = request.getParameter(CommandString.EMAIL);
        String pass = DigestUtils.md5Hex(request.getParameter(CommandString.PASSWORD));
        String name = request.getParameter(CommandString.NAME);
        String address = request.getParameter(CommandString.ADDRESS);
        if (CommandString.ADMIN.equalsIgnoreCase(name)){
            request.setAttribute(CommandString.REGISTRATION_ERROR1, CommandString.REGISTRATION_ERROR2);
            return JspPageName.REGISTRATION_PAGE;
        }

       try {
           customer = DAOCustomer.registerNewCustomer(name, email, pass, address);
           if (customer == null) {
               request.setAttribute(CommandString.REGISTRATION_ERROR, CommandString.REGISTRATION_ERROR2);
               return JspPageName.REGISTRATION_PAGE;
           }
           else if (customer.getName().equalsIgnoreCase(CommandString.ADMIN)) {
               session.setAttribute(CommandString.ADMIN, customer);
           } else {
               session.setAttribute(CommandString.USER, customer);
           }
       }catch (DAOException ex) {
           throw new CommandException(ex.getMessage(),ex);
       }
        return JspPageName.INDEX_PAGE;
    }
}
