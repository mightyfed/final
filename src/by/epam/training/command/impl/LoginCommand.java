package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOCustomer;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.inpl.DAODBCustomer;
import by.epam.training.entity.Customer;
import org.apache.commons.codec.digest.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginCommand extends Command {

	private DAOCustomer DAODBCustomer =new DAODBCustomer();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{
		HttpSession session = request.getSession();
		Customer customer = new Customer();
		customer.setEmail(request.getParameter(CommandString.EMAIL));
		customer.setPassword(DigestUtils.md5Hex(request.getParameter(CommandString.PASSWORD)));
		try {
			customer = DAODBCustomer.getCustomer(customer);
			if (customer == null) {
				request.setAttribute(CommandString.LOGIN_ERROR1, CommandString.LOGIN_ERROR2);
				return JspPageName.LOGIN_PAGE;
			}
			else if (customer.getName().equalsIgnoreCase(CommandString.ADMIN)) {
				session.setAttribute(CommandString.ADMIN, customer);
			} else {
				session.setAttribute(CommandString.USER, customer);
			}
		} catch (DAOException ex) {
			throw new CommandException(ex.getMessage());
		}
		return JspPageName.INDEX_PAGE;
	}
}
