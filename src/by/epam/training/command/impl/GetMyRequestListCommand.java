package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAORequest;
import by.epam.training.dao.inpl.DAODBRequest;
import by.epam.training.entity.Customer;
import by.epam.training.entity.Request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


public class GetMyRequestListCommand extends Command {

	private DAORequest dReq = new DAODBRequest();

	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{
		HttpSession session = request.getSession();

		List<Request> myReq = null;
		Customer customer = (Customer) session.getAttribute(CommandString.USER);

		try {
			myReq = dReq.getMyRequests(customer);
		} catch (DAOException ex) {
			session.setAttribute(CommandString.MY_REQUEST_ERROR_ATTRIBUTE, CommandString.ERROR);
			throw new CommandException(ex.getMessage());
		}

		session.setAttribute(CommandString.MY_REQUEST, myReq);
		return JspPageName.CUSTOMER_PAGE;
	}
}
