package by.epam.training.command.impl;

import by.epam.training.command.Command;
import by.epam.training.command.CommandException;
import by.epam.training.command.CommandString;
import by.epam.training.controller.JspPageName;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAORequest;
import by.epam.training.dao.inpl.DAODBRequest;
import by.epam.training.entity.Request;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


public class GetRequestListCommand extends Command {

	Logger logger = Logger.getLogger(GetRequestListCommand.class);
	private DAORequest dReq = new DAODBRequest();

	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException{
		HttpSession session = request.getSession();

		List<Request> nReq = null;
		List<Request> eReq = null;

		try {
			nReq = dReq.getNewRequests();
		} catch (DAOException ex) {
			logger.error(CommandString.NEW_REQUEST_ERROR, ex);
			throw new CommandException(ex.getMessage());
		}
		try {
			eReq = dReq.getExpiredRequests();
		} catch (DAOException ex) {
			logger.error(CommandString.EXPIRED_REQUEST_ERROR, ex);
			throw new CommandException(ex.getMessage());
		}

		session.setAttribute("nReq", nReq);
		session.setAttribute("eReq", eReq);
		return JspPageName.ADMIN_PAGE;
	}
}
