package by.epam.training.command;

import by.epam.training.command.impl.*;

public class CommandManager {

	private enum Commands {
		WORK, WORKER, MY_REQUESTS, CREATE_REQUEST, SHOW_REQUESTS, ASSIGN_WORKER, LOGIN, LOGOUT, DELETE_REQUEST, REGISTER, CHANGELOC, HOME
	}

	public static Command getCommand(String command) throws CommandException{
		Commands com = Commands.valueOf(command.toUpperCase());
		switch (com) {
		case WORK:
			return new GetWorkListCommand();
		case WORKER:
			return new GetWorkerListCommand();
		case MY_REQUESTS:
			return new GetMyRequestListCommand();
		case CREATE_REQUEST:
			return new CreateRequestCommand();
		case SHOW_REQUESTS:
			return new GetRequestListCommand();
		case ASSIGN_WORKER:
			return new AssignWorkerCommand();
		case LOGIN:
			return new LoginCommand();
		case LOGOUT:
			return new LogoutCommand();
		case DELETE_REQUEST:
			return new DeleteRequestCommand();
		case REGISTER:
			return new RegisterCommand();
		case CHANGELOC:
			return new ChangeLocaleCommand();
			case HOME:
			return new GoHomeCommand();
		default:
			throw  new CommandException("no such command");
		}

	}
}
