package by.epam.training.command;

import javax.servlet.http.*;

public abstract class Command {
    
    public abstract String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
