package by.epam.training.controller;

import by.epam.training.command.CommandException;
import by.epam.training.command.CommandManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet({"/work", "/worker", "/my_requests", "/create_request", "/show_requests", "/assign_worker", "/login", "/logout", "/delete_request", "/register", "/changeloc", "/home"})
public class CommunalServices extends HttpServlet {
    private static Logger logger = Logger.getLogger(CommunalServices.class);
    private static final long serialVersionUID = 3L;


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        perform(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    perform(request,response);
}

    private void perform(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String realPath = request.getServletContext().getRealPath("/");
        PropertyConfigurator.configure(realPath + "WEB-INF//classes//log4j.properties");
        String uri = request.getServletPath();
        String forward = null;
        try {
            forward = CommandManager.getCommand(uri.substring(1)).execute(request, response);
            if (null == forward){
                forward = JspPageName.INDEX_PAGE;
            }
        } catch (CommandException ex){
            logger.error(ex);
            forward = JspPageName.ERROR_PAGE;
        }catch (Exception e){
            logger.error(e.getMessage());
            forward = JspPageName.ERROR_PAGE;
        }
        request.getSession(true).setAttribute("preUrl", forward);
        RequestDispatcher rs = request.getRequestDispatcher(forward);
        rs.forward(request, response);
    }
}
