package by.epam.training.controller;

public final class JspPageName {

	private JspPageName() {}
 	
 	
 	
 	public static final String CUSTOMER_PAGE = "customer.jsp";
	public static final String ERROR_PAGE = "error.jsp";
	public static final String ADMIN_PAGE = "admin.jsp";
	public static final String LOGIN_PAGE = "login.jsp";
	public static final String REGISTRATION_PAGE = "register.jsp";
	public static final String WORK_PAGE = "work.jsp";
	public static final String WORKER_PAGE = "worker.jsp";
	public static final String INDEX_PAGE = "index.jsp";
 	} 	