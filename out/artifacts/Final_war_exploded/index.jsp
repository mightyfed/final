<%@page contentType="text/html;" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.training.resources.content_en_US" />
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><fmt:message key="title.home"/></title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">
</head>

<body>
	<div class="container w60">
		<jsp:include page="header.jsp" />

		<div class="main-container">
			<fmt:message key="home.text"/>
			<c:if test="${not empty user}">, ${user.name}</c:if>
			!
		</div>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>