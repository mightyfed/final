<%@page contentType="text/html; charset =utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="resources.content_en_US" />
<div class="row">
	<div class="col-md-1 logo"></div>
	<div class="col-md-4 textlogo">
		<h1>Communal services</h1>
	</div>
	<div class="col-md-1 col-md-offset-4 signin">
		<button type="button" class="btn btn-info mybtn">
			<c:if test="${empty user && empty admin}">
				<a href="login.jsp">Log in</a>
			</c:if>
			<c:if test="${ not empty user || not empty admin}">
				<a href="logout">Log out</a>
			</c:if>
		</button>
		<c:if test="${empty user && empty admin}">
		<button type="button" class="btn-info mybtn2">
				<a href="register.jsp">Register</a>
		</button>
		</c:if>
	</div>
</div>

<div class="row">
	<nav class="main-menu">
		<ul>
			<li><a href="index.jsp">Home</a></li>
			<c:if test="${not empty user}">
				<li><a href="work">Work</a></li>
			</c:if>
			<c:if test="${not empty admin}">
				<li><a href="worker">Workers</a></li>
			</c:if>
			<c:if test="${not empty user}">
				<li><a href="my_requests">My requests</a></li>
			</c:if>
			<c:if test="${not empty admin}">
				<li><a href="show_requests">All Requests</a></li>
			</c:if>
		</ul>
	</nav>
</div>