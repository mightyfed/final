<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.training.resources.content_en_US" />
<footer>
	<span class="author"><fmt:message key="header.author"/></span>
</footer>