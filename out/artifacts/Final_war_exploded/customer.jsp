﻿<%@page language="java" contentType="text/html" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="by.epam.training.resources.content_en_US" />
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
<title>Customers</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/table.css">
</head>
<body>
	<div class="container w60">
		<jsp:include page="header.jsp" />
		<div class="main-container">
			<div class="date">
				<p>History</p>
				<table class="table table-condensed">
					<tr>
						<th>Id</th>
						<th>Required work</th>
						<th>Required date</th>
					</tr>
					<c:forEach items="${myReq}" var="mreq">
						<tr>
							<td>${mreq.id_request}</td>
							<td>${mreq.reqwork}</td>
							<td>${mreq.reqtime}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="date">
				<p>New request</p>
				<form action="create_request" method="post">
					<select name="work" required="">
						<option disabled selected value="">Choose required work</option>
						<c:forEach items="${work}" var="wk">
						<option>${wk.name}</option>
						</c:forEach>
					</select><br>
					<input type="date" name="date" placeholder="Required date"><br>
					<input type="submit" value="Send">
				</form>
			</div>
			<div class="date">
				<p>Delete request</p>
				<form action="delete_request" method="post">
					<select name="request" required="">
						<option disabled selected value="">Choose required request</option>
						<c:forEach items="${myReq}" var="mrk">
							<option>${mrk.id_request}</option>
						</c:forEach>
					</select>
					<input type="submit" value="Send">
				</form>
			</div>
		</div>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>